package interFaceExam;

public class BearToy implements MoveArmLeg,Misile,Light{
	
	public BearToy() {
		System.out.println("곰돌이입니다.");
		move();
		misile();
		light();
		System.out.println("---------");
	}
	
	@Override
	public void move() {
		System.out.println("팔다리 움직임");
	}
	
	@Override
	public void misile() {
		System.out.println("미사일 발사 불가");
	};
	
	@Override
	public void light() {
		System.out.println("불빛 발사");
	};
	

}
