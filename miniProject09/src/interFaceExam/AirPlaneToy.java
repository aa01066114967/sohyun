package interFaceExam;

public class AirPlaneToy implements MoveArmLeg,Misile,Light{
	
	
	public AirPlaneToy() {
		System.out.println("비행기입니다.");
		move();
		misile();
		light();
		System.out.println("---------");
	}
	
	
	@Override
	public void move() {
		System.out.println("팔다리 움직임 불가");
	}
	
	@Override
	public void misile() {
		System.out.println("미사일 발사");
	};
	
	@Override
	public void light() {
		System.out.println("불빛 발사");
	};
}
