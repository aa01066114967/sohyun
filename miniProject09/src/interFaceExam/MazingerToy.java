package interFaceExam;

public class MazingerToy implements MoveArmLeg,Misile,Light{

	public MazingerToy() {
		System.out.println("마징가입니다.");
		move();
		misile();
		light();
		System.out.println("---------");
	}
	@Override
	public void move() {
		System.out.println("팔다리 움직임");
	}
	
	@Override
	public void misile() {
		System.out.println("미사일 발사 가능");
	};
	
	@Override
	public void light() {
		System.out.println("불빛 발사 못함");
	};
}
