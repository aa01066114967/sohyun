package oop;
import java.util.*;
public class MyStackDelegation {
	
	private ArrayList<String> arList = new ArrayList<String>();
	private Vector<String> vector = new Vector<String>();
	
	public void push(String element) {
		arList.add(element);
		}
	public String pop() {
		return arList.remove(arList.size()-1);
	}
	public boolean isEmpty() {
		return arList.isEmpty();
	}
	public int size() {
		return arList.size();
	}
	
	
	

}
