package starbucksExam;

public class CafeLatte implements HeadStarbucks{
	
	public CafeLatte() {
		System.out.println("신촌점입니다.");
		chai();
		ameriano();
		cafelatte();
	}
	
	@Override
	public void chai() {
		System.out.println("차이티 불가");
	}
		
	@Override
	public void ameriano() {
		System.out.println("아메리카노 불가");
	}
	
	@Override
	public void cafelatte() {
		System.out.println("카페라떼 불가");
		
	}
}
