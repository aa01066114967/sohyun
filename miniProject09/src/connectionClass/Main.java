package connectionClass;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Course se = new Course("Software Engineering");
		Course designPattern = new Course("DesignPattern");

		Student s1 = new Student("ȫ�浿");
		Student s2 = new Student("ȫ�漭");
		Student s3 = new Student("ȫ�泲");
		
		Transcript t1 = new Transcript(s1, se); //ȫ�浿�� ����Ʈ���� ����
		Transcript t2 = new Transcript(s2, designPattern);
		Transcript t3 = new Transcript(s3, designPattern);
		
		//s1.addTranscript();
		
		se.printCourse();
		designPattern.printCourse();
		
		s1.printStudent();
		s2.printStudent();
		s3.printStudent();
	
		t1.setGrade("A+");
		t2.setGrade("C+");
		t3.setGrade("A+");
		
		t1.setData("2020��");
		t2.setData("2020��");
		t3.setData("2020��");
		
		
		t1.printTranscript();
		t2.printTranscript();
		t3.printTranscript();
	}

}
