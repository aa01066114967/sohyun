package connectionClass;

import java.util.ArrayList;
public class Course {
	
	public String name;
	public ArrayList<Student> students;
	public ArrayList<Transcript> transcript;
	
	public Course(String name) {
		this.name = name;
		ArrayList<Student> students = new ArrayList<>();
		ArrayList<Transcript> transcript = new ArrayList();
	}

	public void addTranscript(Transcript addTranscript) {  //수강하다
		transcript.add(addTranscript);
	}//수강 추가
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Student> getStudents() {
		return students;
	}
	
	public void setStudents(ArrayList<Student> students) {
		this.students = students;
	}
	
	public void printCourse() {
		System.out.println(name);
	}
	
	
	public void addStudent(Student addStudent) {
		students.add(addStudent);
	}
	
	public void removeStudent(Student removeStudent) {
		if(students.contains(removeStudent)) {
			students.remove(removeStudent);
		}else {
			System.out.println("학생이 없습니다.");	
		}
	}
}
