package connectionClass;

public class Transcript {

	private String date;
	private String grade;
	private Course course;
	private Student student;
	
	public Transcript(Student student, Course course) {
		this.course = course;
		this.student = student;
	}
	
	public Student getStudent() {
		return student;
	}
	public Course getCourse() {
		return course;
	}
	public void setData(String date) {
		this.date = date;
	}
	public String getDate() {
		return date;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getGrade() {
		return grade;
	}
	public void printTranscript() {
		System.out.println(date + " "+ student.getName() +"  "+ course.getName() + " " + getGrade()); //public을 해서 .name가져올수있었고, private하면 get으로 가져와야함
	}
	
}
