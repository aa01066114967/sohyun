package connec;

public class Person {

	private Phone homePhones;
	private Phone officePhone;
	
	public Phone getHomePhones() {
		return homePhones;
	}
	public Phone getOfficePhone() {
		return officePhone;
	}
	public void setHomePhone(Phone phone) {
		this.homePhones = phone;
	}
	public void setOfficePhone(Phone phone) {
		this.officePhone = phone;
	}
	
}
