package exam02_interface;

public class RemoteControlExample02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		RemoteControl rc;
		rc = new Television();
		
		rc.setMute(true);
		rc.turnOn();
		rc.turnOff();
		System.out.println("-------------");
		
		rc = new Audio();
		rc.setVolume(11);
		rc.setMute(true);
		rc.turnOn();
		rc.turnOff();

	}

}
