package exam02_interface;

public class RemoteControlExample {
	
	public static void main(String[] args) {
		
		RemoteControl rc = new RemoteControl(){
			
			public void turnOn() {
				System.out.println("오디오 : ");
				System.out.println("Power Turn On");
			}
			public void turnOff() {
				System.out.println("오디오 : ");
				System.out.println("Power Turn Off");
			}
			public void setVolume(int volume) {
				System.out.println("볼륨은 " + volume);
			}//End of setVolume
				
		};
		
		rc.turnOn();
		rc.turnOff();
		rc.setVolume(15); //볼륨 입력은 여기서
		RemoteControl.changeBattery();
		rc.setMute(true);
	}
}
