package miniProject09;

public class CustomerContact extends Contact {

	public CustomerContact(String name, String phoneNumber, String email, String address, String birthday, String group,
			String customer, String item, String positionCustomer) {
		super(name, phoneNumber, email, address, birthday, group);
		this.customer = customer;
		this.item = item;
		this.positionCustomer = positionCustomer;
	}

	String customer;
	String item;
	String positionCustomer;

	@Override
	public void printInfo() {
		System.out.println("�̸� : " + this.getName());
		System.out.println("��ȭ��ȣ : " + this.getPhoneNumber());
		System.out.println("�̸��� : " + this.getEmail());
		System.out.println("�ּ� : " + this.getAddress());
		System.out.println("���� : " + this.getBirthday());
		System.out.println("�׷� : " + this.getGroup());
		System.out.println("ȸ���̸� : " + this.customer);
		System.out.println("�μ��̸� : " + this.item);
		System.out.println("����  : " + this.positionCustomer);
	}

}
